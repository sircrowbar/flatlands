// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!

README
	For greater enjoyment and ingestion, please look at the following safety document to get acquainted with your new tiny world.
	Failure to do so may reduce enjoyment and is certainly not a way to make up for the fact that I ran out of time prior to
	

About!:
	Flatlands is a game with an ever changing world! You control a very tiny world (9x9 grid) and can place various tiles on any of
	the 9 places available to you. But be careful! As you're placing tiles, parts of the world have a tendacy to fall off! Every
	5 turns or so, select columns from 1 to all my fall of the edge from the top! To reach the win condition, you're going to have 
	to manically modify your land fast to reach the world state you so desire! 

Controls!:
	
	Mouse Base
	- Select your mode by the buttons on the right side buttons via left click (grouped in a 4x4 grid).
	- Then select anywhere on the gird by left click. Depending on the mode, certain actions will happen!
	- Press M to Mute the Music
	- Press R to reset the game at anytime.
	- Press Space to pause, but... Honestly, not really sure why I implementing that now. Huh.
	
Notes!:
	- Columns in with orange warnings on them are the next to fall! Plan carefully!
	- Movement can only be done on adjacent tiles. Or should be. Maybe that broke again. Let's find out!
	- Apparently, cities and human settlements split off via mitosis: If you move a town, only half of the town leaves with you, so you're
	  left with two villages there. Settlements simply move over. Use this to your (dis)advantage.
	-At this time, Upgrades can only be done on Settlements and Human related things (Settlement to Village to Town to City). I had some
	other ideas and a tech tree lined up for Grass, but I simply ran out of time. I briefly thought about adding terrible things to 
	the water tech tree, but honestly I only thought about that while typing this up. Yay stream of conscious!
	- 

Tools!:
	- Microsoft Visual Studio Express 2010 with XNA 4.0
	- pxTone
	- Paint.NET
	- Spotify for m00sicks. ( http://open.spotify.com/user/sircrowbar/playlist/4jwijLZlffoYLo6Q8ESZ2R )
	- A very noisy house where work on the bathroom is being done. Aggghhhh.
	- My bed. My comfy, comfy bed.

To Do!:
	Lots and lots of things. I had a lot of plans for stuff for tiles that cause effects on some tiles or
	across the entire board and I never got to to them. It's a shame, because I think it would have made the
	game a whole lot more fun. The good news is that I like this idea enough to keep working on it beyond
	Ludum Dare, so who knows! MAYBE THERE WILL BE MORE.
	
	If you liked this or liked the idea where this was going, please let me know. I'd like to know if I'm
	on the right track or not with this. Thank you!

Bugs!:
	Probably plenty. Let me know if you find any!
	
Contact!:
	If you want to contact me with questions about this game or just want to chat about general things,
	here are some easy ways to contact me!
	
	E-Mail: sircrowbar@gmail.com
	Twitter: JohannHat
	Xbox: SirCrowbar
	PS3: JohannHat (Yeah, I know, I'm being difficult.)
	Real life: Shout very loudly at the guy with long hair.
	
	I don't bite, really!