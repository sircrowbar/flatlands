// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;


namespace Flatlands
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class DebugManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        bool debugActive = false;

        SpriteBatch spriteBatch;
        SpriteFont font;

        bool debugButtonPressed = false;

        private float elapsedTime, totalFrames, fps;

        public bool getDebug
        {
            get { return debugActive; }
        }

        public DebugManager(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }


        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Game.Content.Load<SpriteFont>(@"Fonts\DebugFont");
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            if (!debugActive) return;

            totalFrames++;
            string test = "FPS: " + fps.ToString();
            spriteBatch.Begin();
            spriteBatch.DrawString(font, test, new Vector2(0, 0), Color.Red, 0, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0.0f);
            //spriteBatch.Draw(line, Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero, 200.0f, SpriteEffects.None, 0);                
            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();

            if (keyState.IsKeyDown(Keys.Back) && !debugButtonPressed)
            {
                setDebug();
                debugButtonPressed = true;
            }

            if (keyState.IsKeyUp(Keys.Back))
            {
                debugButtonPressed = false;
            }

            if (!debugActive) return;

            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (elapsedTime >= 1.0f)
            {
                fps = totalFrames;
                totalFrames = 0;
                elapsedTime = 0;
            }

            base.Update(gameTime);
        }

        public void setDebug()
        {
            debugActive = (debugActive) ? false : true;
        }
    }
}