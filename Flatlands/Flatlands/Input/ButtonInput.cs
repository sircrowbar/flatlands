﻿// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Flatlands.Input
{
    class ButtonInput
    {
        public Keys keyButton;
        public Buttons padButton;
        bool pressed = false;

        public bool isPressed { get { return pressed; } }

        //For Mouse Input
        public ButtonInput()
        {
        }

        public ButtonInput(Keys key, Buttons button)
        {
            keyButton = key;
            padButton = button;
        }

        public bool press(KeyboardState keyState, GamePadState padState)
        {
            bool justPressed = false;

            if ((keyState.IsKeyDown(keyButton) || padState.IsButtonDown(padButton)) && pressed == false)
            {
                pressed = true;
                justPressed = true;
            }

            if (keyState.IsKeyUp(keyButton) && padState.IsButtonUp(padButton))
            {
                pressed = false;
            }

            return justPressed;

        }


        public bool leftClick(MouseState mouseState)
        {
            bool justPressed = false;

            if (mouseState.LeftButton == ButtonState.Pressed && pressed == false)
            {
                pressed = true;
                justPressed = true;
            }

            if (mouseState.LeftButton == ButtonState.Released)
            {
                pressed = false;
            }


            return justPressed;

        }


        public bool rightClick(MouseState mouseState)
        {
            bool justPressed = false;

            if (mouseState.RightButton == ButtonState.Pressed && pressed == false)
            {
                pressed = true;
                justPressed = true;
            }

            if (mouseState.RightButton == ButtonState.Released)
            {
                pressed = false;
            }


            return justPressed;

        }  


    }
}
