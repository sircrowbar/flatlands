﻿// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flatlands
{
    public enum ButtonType { Build, Destroy, Upgrade, Move };

    class Button
    {
        public ButtonType type;
        protected string asset;
        protected Vector2 pos;
        public Vector2 Position
        {
            get { return pos; }
            set
            {
                pos = value;
                updateHitBox();
            }
        }
        public Rectangle hitBox;
        protected Texture2D tex;

        public Button(ButtonType type)
        {
            this.type = type;
        }

        public void load(ContentManager content)
        {
            asset = @"Buttons\" + type.ToString();
            tex = content.Load<Texture2D>(asset);
            hitBox.Width = tex.Width;
            hitBox.Height = tex.Height;
        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(tex, Position, null, Color.Pink, 0.0f, new Vector2(0), 1.0f, SpriteEffects.None, 0.0f);
        }

        public void update(GameTime gameTime)
        {

        }

        public void updateHitBox()
        {
            hitBox.X = (int)Position.X;
            hitBox.Y = (int)Position.Y;
        }
    }
}
