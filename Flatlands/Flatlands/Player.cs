﻿// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

using Flatlands.Tiles;


namespace Flatlands
{
    class Player
    {
        public ButtonType mode = ButtonType.Build;

        public int Population { get; set; }
        public int Nature { get; set; }
        public int Culture { get; set; }

        public Player()
        {
            Population = 0;
            Nature = 0;
            Culture = 0;
        }

        public void update()
        {
        }

        public void changeMode(ButtonType type)
        {
            mode = type;
        }

    }
}
