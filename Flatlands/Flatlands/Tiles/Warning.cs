﻿// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flatlands.Tiles
{
    class Warning
    {
        protected Vector2 pos;
        protected Texture2D texOff;
        protected Texture2D texOn;
        protected bool activated = false;

        public Warning(Vector2 pos)
        {
            this.pos = pos;
        }

        public void load(ContentManager content)
        {
            texOff = content.Load<Texture2D>(@"Tiles\warningoff");
            texOn = content.Load<Texture2D>(@"Tiles\warningon");
        }

        public void clear()
        {
            activated = false;
        }

        public void toggle()
        {
            activated = (activated) ? false : true;
        }

        public void draw(SpriteBatch batch)
        {
            if(activated)
                batch.Draw(texOn, pos, null, Color.Pink, 0.0f, new Vector2(0), 1.0f, SpriteEffects.None, 0.0f);
            else
                batch.Draw(texOff, pos, null, Color.Pink, 0.0f, new Vector2(0), 1.0f, SpriteEffects.None, 0.0f);
        }

    }
}
