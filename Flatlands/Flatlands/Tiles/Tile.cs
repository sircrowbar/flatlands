﻿// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flatlands.Tiles
{
    class Tile
    {
        public enum TileTypes { Grass, Water, Settlement, Village, Town, City, Park };
        const float MAX_MOVE = 100;
        const float VELOCITY = 200;
        

        //Columns always move vertically
        public float currMove = 0.0f;
        public bool move = false;
        public bool dead = false;
        public float scale = 1.0f;


        public int Population = 0;
        public int Nature = 0;
        public int Culture = 0;

        public TileTypes tile;

        protected string asset;
        protected Vector2 pos;
        public Vector2 Position
        {
            get { return pos; }
            set
            {
                pos = value;
                updateHitBox();
            }
        }
        protected Vector2 gridPosition = Vector2.Zero;
        public Rectangle hitBox = Rectangle.Empty;
        protected Texture2D tex;
        public bool upgradeable = false;

        public Tile()
        {
            tile = RandomTile();
            Position = Vector2.Zero;
            hitBox.X = (int)Position.X;
            hitBox.Y = (int)Position.Y;
            if (tile == TileTypes.Settlement || tile == TileTypes.Village || tile == TileTypes.Town || tile == TileTypes.City)
            {
                upgradeable = true;
            }
            setInfo();

        }

        public Tile(TileTypes type)
        {
            tile = type;
            Position = Vector2.Zero;
            hitBox.X = (int)Position.X;
            hitBox.Y = (int)Position.Y;
            if (tile == TileTypes.Settlement || tile == TileTypes.Village || tile == TileTypes.Town || tile == TileTypes.City)
            {
                upgradeable = true;
            }
            setInfo();
        }

        public void setInfo()
        {
            switch (tile)
            {
                case TileTypes.Settlement:
                    Population = 100;
                    Culture = 10;
                    Nature = 0;
                    break;
                case TileTypes.Village:
                    Population = 200;
                    Culture = 15;
                    Nature = 0;
                    break;
                case TileTypes.Town:
                    Population = 400;
                    Culture = 30;
                    Nature = 0;
                    break;
                case TileTypes.City:
                    Population = 800;
                    Culture = 50;
                    Nature = 0;
                    break;
                case TileTypes.Water:
                     Population = 0;
                    Culture = 0;
                    Nature = 100;
                   break;
                case TileTypes.Grass:
                   Population = 0;
                   Culture = 0;
                   Nature = 100;
                   break;
                case TileTypes.Park:
                   Population = 0;
                   Culture = 10;
                   Nature = 25;
                   break;
                //case TileTypes.Farm:
                //   Population = 10;
                //   Culture = 5;
                //   Nature = 50;
                //   break;
            }
        }

        public void load(ContentManager content)
        {
            asset = @"Tiles\" + tile.ToString();
            tex = content.Load<Texture2D>(asset);
            hitBox.Width = tex.Width;
            hitBox.Height = tex.Height;
        }

        public void load(ContentManager content, TileTypes type)
        {
            tile = type;
            asset = @"Tiles\" + tile.ToString();
            tex = content.Load<Texture2D>(asset);
            hitBox.Width = tex.Width;
            hitBox.Height = tex.Height;
        }

        protected TileTypes RandomTile()
        {   
            Random rnd = new Random();
            
            //TileTypes type = (TileTypes)rnd.Next(0, Enum.GetValues(typeof(TileTypes)).Length);
            TileTypes type = (TileTypes)rnd.Next(0, 3);


            return type;
        }

        public void draw(SpriteBatch batch)
        {
            batch.Draw(tex, Position, null, Color.Pink, 0.0f, new Vector2(0), scale, SpriteEffects.None, 0.0f);
        }

        public void update(GameTime gameTime)
        {
            if (move){
                //currMove += VELOCITY;
                float nowMove = (float)gameTime.ElapsedGameTime.TotalSeconds * VELOCITY;
                currMove += nowMove;
                pos.Y -= nowMove;
                updateHitBox();
                if (currMove >= MAX_MOVE)
                {
                    move = false;
                    currMove = 0.0f;
                }
            }

            if (dead)
            {
                if (scale >= 0.0f)
                {
                    scale -= 0.05f;
                }
            }


        }

        public void upgrade(ContentManager content)
        {
            bool upgrade = false;
            switch (tile)
            {
                case TileTypes.Settlement:
                    tile++;
                    upgrade = true;
                    break;
                case TileTypes.Village:
                    tile++;
                    upgrade = true;
                    break;
                case TileTypes.Town:
                    tile++;
                    upgrade = true;
                    break;
                case TileTypes.City:
                    break;
                case TileTypes.Grass:
                    tile = TileTypes.Park;
                    upgrade = true;
                    break;
            }

            if (upgrade)
            {
                asset = @"Tiles\" + tile.ToString();
                tex = content.Load<Texture2D>(asset);
                hitBox.Width = tex.Width;
                hitBox.Height = tex.Height;
                setInfo();
            }
        }



        public bool downgrade(ContentManager content)
        {
            bool downgrade = false;
            switch (tile)
            {
                case TileTypes.Settlement:
                    break;
                case TileTypes.Village:
                    tile--;
                    downgrade = true;
                    break;
                case TileTypes.Town:
                    tile--;
                    downgrade = true;
                    break;
                case TileTypes.City:
                    tile--;
                    downgrade = true;
                    break;
                case TileTypes.Park:
                    tile = TileTypes.Grass;
                    downgrade = true;
                    break;
            }

            if (downgrade)
            {
                asset = @"Tiles\" + tile.ToString();
                tex = content.Load<Texture2D>(asset);
                hitBox.Width = tex.Width;
                hitBox.Height = tex.Height;
                setInfo();
            }

            return downgrade;
        }

        public void updateHitBox()
        {
            hitBox.X = (int)Position.X;
            hitBox.Y = (int)Position.Y;
        }
    }
}
