﻿// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flatlands
{
    public class BGLayer
    {
        string name;
        Texture2D bg;
        bool scroll = false;
        bool parallax = false;
        Vector2 velocity = Vector2.Zero;
        Vector2 pos = Vector2.Zero;

        public BGLayer(string tempName)
        {
            name = tempName;
        }


        public BGLayer(string tempName, bool tempScroll, bool tempParallax, Vector2 tempScrollVel)
        {
            name = tempName;
            scroll = tempScroll;
            parallax = tempParallax;
            velocity = tempScrollVel;
        }

        public void load(ContentManager content)
        {
            bg = content.Load<Texture2D>(name);
        }

        public void draw(SpriteBatch batch, Rectangle size)
        {

            for (int i = size.Y; i < size.Height; i += bg.Height)
            {
                for (int j = size.X; j < size.Width; j += bg.Width)
                {
                    batch.Draw(bg, new Vector2((float)j, i) + pos, null, Color.Pink, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1);
                }
            }

        }

        public void update(Vector2 camMove)
        {
            if (scroll)
            {
                pos += velocity;
                if (Math.Abs(pos.X) > bg.Width)
                {
                    pos.X = 0;
                }

                if (Math.Abs(pos.Y) > bg.Height)
                {
                    pos.Y = 0;
                }
            }

            if (parallax)
            {
                if (camMove.X > 0)
                {
                    pos.X += velocity.X;
                }
                else if (camMove.X < 0)
                {
                    pos.X -= velocity.X;
                }

                if(camMove.Y > 0){
                    pos.Y += velocity.Y;
                }else if(camMove.Y < 0){
                    pos.Y -= velocity.Y;
                }

                if (Math.Abs(pos.X) > bg.Width)
                {
                    pos.X = 0;
                }

                if (Math.Abs(pos.Y) > bg.Height)
                {
                    pos.Y = 0;
                }
            }
        }

    }
}
