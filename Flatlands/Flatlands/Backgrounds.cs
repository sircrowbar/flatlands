﻿// Game Name: Flatlands
// Author: John Hurst (sircrowbar@gmail.com) - Crowbar / Johannhat - http://www.badenoughdude.com/
// Started: 4/20/2012
// Last Updated: 4/22/2012
// For Ludum Dare 23!
// Holy crap I forgot to comment the shit out of everything. The good news is that I plan on expanding this game
// further so maybe there will be time for that afterwards! Happy Ludum Day! - JH

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace Flatlands
{
    public class Backgrounds
    {
        List<BGLayer> bgList = new List<BGLayer>();
        Rectangle size;

        public Rectangle setSize
        {
            set { size = value; }
        }

        public List<BGLayer> list
        {
            set { bgList = value; } 
            get { return bgList; }
        }

        public Backgrounds()
        {
 
        }

        public void addBG(BGLayer temp)
        {
           bgList.Add(temp);
        }

        public void load(ContentManager content)
        {
            foreach (BGLayer temp in bgList)
            {
                temp.load(content);
            }
        }

        public void draw(SpriteBatch batch)
        {
            foreach (BGLayer temp in bgList)
            {
                temp.draw(batch, size);
            }
        }

        public void update(Vector2 camMove)
        {
            foreach (BGLayer temp in bgList)
            {
                temp.update(camMove);
            }
        }

    }
}
